#!/usr/bin/env bash


#minikube start
#minikube docker-env
#eval $(minikube docker-env)

docker build -t frontend:1.0 frontend/.
docker build -t backend_2:1.0 backend_2/.
docker build -t backend_1:1.0 backend_1/.
docker build -t prometheus:1.0 prometheus/.


for i in $(ls  manifests/); do  kubectl create -f manifests/$i  ; done
