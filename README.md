# Описание
Запускаются 3 ВМ в minikube. 
* Frontend
* Backend1
* Backend2
Скрипт ./script.sh  следит за количеством pod backend-ов путем опроса кластера средствами kubectl и количеством установленных соединений TCP Established путем опроса Prometheus. 
Задача всегда держать количество pod backend-ов на 1 больше чем установленных соединений tcp.

## Требования:
* minikube version: v1.22.0
* jq-1.6


## Подготовка к работе
### Создание виртуального окружения
```
minikube start --extra-config=apiserver.service-node-port-range=3080-3090
minikube docker-env
eval $(minikube -p minikube docker-env)
```
Запускаем скрипт ./build.sh, который создаст образы frontend:1.0 backend_1:1.0 backend_2:1.0 и prometheus:1.0. А так-же создаст services: frontend, backend1, backend2 в кластере minikube. 
Для работоспособности приложения необходимо отредактировать файл index.html в контейнере frontend и подставить свои URL backend-ов

![](https://i.imgur.com/J4XvqQi.png)

kubectl exec --stdin --tty frontend-5fdcb97b6-zv77w -- bash

Настройка Prometheus
На pod frontend необходимо установит и запустить [node exporter](https://github.com/prometheus/node_exporter/releases/download/v1.2.2/node_exporter-1.2.2.linux-amd64.tar.gz) и
 добавить в файл prometheus/prometheus.yml
IP своего pod frontend
![](https://i.imgur.com/2Q0vNLt.png)

После чего создаем сервис Prometheus командой
```
kubectl create -f prometheus/depl.yml
```
UI Prometheus доступен по URL из вывода minikube service list
Провека target 
![](https://i.imgur.com/lsH60CM.png)

## Проверка работы

Запускаем скрипт ./script.sh

Для увеличения установленных соединений можно использовать 
утилиту Netcat.

![](https://i.imgur.com/CR5mzT1.png)

![](https://i.imgur.com/jFbmSv3.png)

## Разрушение виртуального окружения
```
minikube delete --all

```


