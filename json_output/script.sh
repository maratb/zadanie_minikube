#!/usr/bin/env bash

#BACKEND1_REPLICS=$(kubectl get deployment backend1 -o jsonpath='{.status.availableReplicas}')
#BACKEND2_REPLICS=$(kubectl get deployment backend2 -o jsonpath='{.status.availableReplicas}')

#FRONTEND_TCP_ESTABL=$(curl 'http://192.168.49.2:3081/api/v1/query?query=node_netstat_Tcp_CurrEstab' | jq -r '.data.result[0].value[1]')

# echo $BACKEND1_REPLICS $BACKEND2_REPLICS $FRONTEND_TCP_ESTABL

while true
do
  BACKEND1_REPLICS=$(kubectl get deployment backend1 -o jsonpath='{.status.availableReplicas}')
  BACKEND2_REPLICS=$(kubectl get deployment backend2 -o jsonpath='{.status.availableReplicas}')
  FRONTEND_TCP_ESTABL=$(curl -s 'http://192.168.49.2:3081/api/v1/query?query=node_netstat_Tcp_CurrEstab' | jq -r '.data.result[0].value[1]')
  
  echo "backend1= "$BACKEND1_REPLICS "backend2= "$BACKEND2_REPLICS "tcp= "$FRONTEND_TCP_ESTABL
  sleep 2
  if [[ $BACKEND1_REPLICS -ne $(( $FRONTEND_TCP_ESTABL + 1 )) ]]
    then
      kubectl scale deployment backend1 --replicas=$(( $FRONTEND_TCP_ESTABL + 1 ))
      kubectl scale deployment backend2 --replicas=$(( $FRONTEND_TCP_ESTABL + 1 ))
  else
    echo "Works!"
  fi
done
